package com.example.spring_homework_002.repository;

import com.example.spring_homework_002.model.entity.Product;
import com.example.spring_homework_002.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {
    // select all product
    @Select("SELECT * FROM product")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productId", column = "product_Id")
    @Result(property = "price", column = "product_price")
    List<Product> findAllProduct();

    // select by id
    @Select("SELECT *FROM product WHERE product_id = #{productId}")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productId", column = "product_Id")
    @Result(property = "price", column = "product_price")
    Product getProductById(Integer productId);


    // delete product

    @Delete("DELETE FROM product WHERE product_id = #{productId}")
    boolean deleteProductById(Integer productId);

    @Select("INSERT INTO product (productName, price) VALUES(#{request.productName}, #{request.price})")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productId", column = "product_Id")
    @Result(property = "price", column = "product_price")
    Integer saveProduct(@Param("request") ProductRequest productRequest);


}
