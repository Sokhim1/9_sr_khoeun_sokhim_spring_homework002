package com.example.spring_homework_002.controller;

import com.example.spring_homework_002.model.entity.Product;
import com.example.spring_homework_002.model.request.ProductRequest;
import com.example.spring_homework_002.model.response.ProductResponse;
import com.example.spring_homework_002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")

public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping("/all")
    @Operation(summary = "Get all product")
    public ResponseEntity<ProductResponse<List<Product>>>getAllProduct(){

        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .message("Fetch successful")
                .payload(productService.getAllProducts())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();

        return ResponseEntity.ok(response);

    }

    @GetMapping("/{id}")
    @Operation(summary = "Get product by id")

    public  ResponseEntity <ProductResponse<Product>> getProductById(@PathVariable("id") Integer productId){

        ProductResponse<Product> response = null;
        if (productService.getProductByID(productId) != null){
            response = ProductResponse.<Product>builder()
                    .message("Success fetch data by id")
                    .payload(productService.getProductByID(productId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<Product>builder()
                    .message("Data not foundd")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);


        }
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "delete product by id")
    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable ("id")Integer productId){
        ProductResponse<String> response = null;
        if (productService.deleteProductByID(productId) == true){
            response = ProductResponse.<String>builder()
                    .message("Delete successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }

        return ResponseEntity.ok(response);

    }

    @PutMapping
    @Operation(summary = "Save new product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest){

        Integer storeProductId = productService.addNewProduct(productRequest);

        if ( storeProductId != null){
            ProductResponse<Product> response =  ProductResponse.<Product>builder()
                    .message("Add success")
                    .payload(productService.getProductByID(storeProductId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
}
