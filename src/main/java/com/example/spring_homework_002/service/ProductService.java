package com.example.spring_homework_002.service;

import com.example.spring_homework_002.model.entity.Product;
import com.example.spring_homework_002.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();

    Product getProductByID(Integer productId);

    boolean deleteProductByID(Integer productId);

    Integer addNewProduct(ProductRequest productRequest);


}
