package com.example.spring_homework_002.service.serviceImp;

import com.example.spring_homework_002.model.entity.Product;
import com.example.spring_homework_002.model.request.ProductRequest;
import com.example.spring_homework_002.repository.ProductRepository;
import com.example.spring_homework_002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    // fin all product
    public List<Product> getAllProducts() {
        return productRepository.findAllProduct();

    }

    @Override
    public Product getProductByID(Integer productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public boolean deleteProductByID(Integer productId) {


        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        Integer productId = productRepository.saveProduct(productRequest);
        return productId;

//        return productRepository.saveProduct(productRequest);
    }

}
